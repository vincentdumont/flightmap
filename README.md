# Flight Map

The world map with flight routes can be generated using the following command:

```
./mapflight.py routes.txt -a -r c -d 100 -i 10
```

All the arguments are optional. Use the `--help` command for more information. Below is an example of a rotating Earth with flight routes produced by the above command:

<p align="center">
<img src="flights.gif" width="80%" />
</p>

Here's another example if we don't use the animation and increase the map resolution to intermediate (`-r i`):

<p align="center">
<img src="flights.png" width="80%" />
</p>

## Basemap library

[This basemap library](https://gitlab.com/vincentdumont/basemap) is needed to run the script. This is a mirror repository from the [official matplotlib library](https://github.com/matplotlib/basemap) with the little addition where the [Night Lights](https://earthobservatory.nasa.gov/features/NightLights) world map backgrounds from NASA can be used. The library can be install easily via `pip` as follows:

```
pip install -U git+https://gitlab.com/vincentdumont/basemap.git
```

Here's an example how the the figure with the black marble background can be generated:

``` python
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
fig = plt.figure(figsize=(12,6),dpi=300)
m = Basemap(projection='moll',resolution='i',lat_0=0,lon_0=0)
m.drawmapboundary(linewidth=0)
m.dnbmap()
plt.show()
```
