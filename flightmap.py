#!/usr/bin/env python
import numpy,os,argparse
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap

argparser = argparse.ArgumentParser(description="Draw flight routes in world map.")
argparser.add_argument('routes', help="Input list of flight routes")
argparser.add_argument('-a','--animation',action='store_true', help="Create rotating Earth animation")
argparser.add_argument('-d',dest='definition',default=300,type=int,help="Figure definition in dpi")
argparser.add_argument('-i',dest='interval',default=5,type=int,help="Angle separation between frames")
argparser.add_argument('-r',dest='resolution',default='i',choices=['c','l','i','h','f'],help="Map resolution")
args = argparser.parse_args()

airports={'AAR':[  10.6281, 56.3075], # Aarhus
          'AKL':[ 174.7850,-37.0082], # Auckland
          'ATH':[  23.9484, 37.9356], # Athens
          'BCN':[   2.0833, 41.2974], # Barcelona
          'BJX':[-101.4806, 20.9935], # Leon
          'BKK':[ 100.7501, 13.6900], # Bangkok
          'BOG':[ -74.1445,  4.7014], # Bogota
          'BRU':[   4.4856, 50.9010], # Brussels
          'CDG':[   2.5479, 49.0097], # Paris (Charles de Gaulle)
          'CPH':[  12.6508, 55.6180], # Copenhagen
          'CUN':[ -86.8736, 21.0403], # Cancun
          'DBX':[  55.3657, 25.2532], # Dubai
          'DCA':[ -77.0402, 38.8512], # Washington DC (Ronald Reagan)
          'EZE':[ -58.5348,-34.8150], # Buenos Aires
          'FRA':[   8.5622, 50.0379], # Frankfurt
          'HER':[  25.1747, 35.3396], # Heraklion
          'GDL':[-103.3076, 20.5260], # Guadalajara
          'GRX':[  -3.7780, 37.1877], # Granada
          'HNL':[-157.9251, 21.3245], # Honolulu
          'ITO':[-155.0417, 19.7188], # Hilo
          'JFK':[ -73.7781, 40.6413], # New York City
          'LAS':[-115.1537, 36.0840], # Las Vegas
          'LAX':[-118.4085, 33.9416], # Los Angeles
          'LGW':[   0.1821, 51.1537], # London (Gatwick)
          'LHR':[   0.4543, 51.4700], # London (Heathrow)
          'LIM':[ -77.1120,-12.0241], # Lima
          'LYS':[   5.0888, 45.7234], # Lyon
          'NCE':[   7.2148, 43.6598], # Nice
          'MAD':[  -3.5676, 40.4983], # Madrid
          'MEL':[ 144.8410,-37.6690], # Melbourne
          'MEX':[ -99.0719, 19.4361], # Mexico City
          'MID':[ -89.6604, 20.9338], # Merida
          'RNO':[-119.7681, 39.4996], # Reno-Tahoe
          'OAK':[-122.2197, 37.7126], # Oakland
          'ORD':[ -87.9073, 41.9742], # Chicago (O'Hare)
          'OSL':[  11.1004, 60.1976], # Oslo
          'PHX':[-112.0078, 33.4373], # Phoenix
          'PMC':[ -73.0985,-41.4337], # Puerto Montt
          'PTY':[ -79.3874,  9.0667], # Panama City
          'PUQ':[ -70.8431,-53.0051], # Punta Arenas
          'RAK':[  -8.0211, 31.6048], # Marrakech
          'SAF':[-106.0845, 35.6183], # Santa Fe
          'SAN':[-117.1933, 32.7338], # San Diego
          'SCL':[ -70.7944,-33.3898], # Santiago de Chile
          'SFO':[-122.3790, 37.6213], # San Francisco
          'STN':[   0.2389, 51.8860], # London (Stansted)
          'SYD':[ 151.1753,-33.9399], # Sydney
          'SZZ':[  14.9028, 53.5859], # Szczecin
          'TIJ':[-116.9717, 32.5423], # Tijuana
          'ZRH':[   8.5555, 47.4582]} # Zurich

routes = numpy.loadtxt(args.routes,dtype=str)
angles = range(0,360,args.interval) if args.animation else [0]
proj = 'ortho' if args.animation else 'moll'

for i in angles:
    print(i)
    fig = plt.figure(figsize=(12,6),dpi=args.definition)
    m = Basemap(projection=proj,resolution=args.resolution,lat_0=0,lon_0=i)
    m.drawmapboundary(linewidth=0)
    for fin,fout in routes:
        m.drawgreatcircle(*airports[fin],*airports[fout],linewidth=0.3,color='white')
    for key in routes.flatten():
        m.plot(*m(*airports[key]),'o',markerfacecolor='black',markersize=3,
               markeredgecolor='white',markeredgewidth=0.7)
    m.dnbmap()
    plt.tight_layout()
    fname = 'img%03i.png'%i if args.animation else 'flights.png'
    plt.savefig(fname)
    plt.close()

if args.animation:
    os.system('convert -delay 20 -alpha background -loop 0 *.png flights.gif && rm *.png')
